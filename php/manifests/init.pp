class php
{
  $version = hiera('php::version')

  # Define PHP Packages

  if ($::operatingsystem == 'CentOS' and $version == '5.6') {
    $packageList = hiera('php::packages::56')
  } else {
    $packageList = hiera('php::packages::54')
  }

  # Define apache service and if OS is CentOS, install repo's
  if ($::operatingsystem == 'CentOS') {
    $apache = 'httpd'

    yumrepo { 'epel':
      name      => 'epel-release-7-5.noarch.rpm',
      baseurl   => 'http://dl.fedoraproject.org/pub/epel/7/x86_64/',
      descr     => 'Epel Release',
      enabled   => 1,
      gpgcheck  => 0
    }->

    yumrepo { 'ius-repo':
      name => 'ius-release-1.0-14.ius.centos7.noarch.rpm',
      baseurl   => 'https://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64',
      descr     => 'IUS repo',
      enabled   => 1,
      gpgcheck  => 0,
      notify    => Exec['update-os']
    }

    package { $packageList:
      ensure     => 'present',
      notify     => Service['httpd'],
      require => Yumrepo['ius-repo']
    }
  } else {
    if ($::operatingsystem == 'Ubuntu') {
      package { $packageList:
        ensure     => 'present',
        notify     => Service['apache2'],
      }
    }
  }

  # Set permissions to webuser, for the PHP sessions directory
  file { '/var/lib/php/session':
    ensure => directory,
    owner => hiera('httpd::user')
  }
}