class ntp {
  package {[
    'ntp',
    'ntpdate',
    'ntp-doc'
  ]:
  ensure => present}

  $timezone = hiera('ntp::timezone')

  # Enable NTP Service
  service { 'ntpdate':
    ensure => running,
    require => Package['ntpdate']
  }

  exec { "set-timezone-${timezone}":
    command => "/usr/bin/timedatectl set-timezone ${timezone}",
    require => Service['ntpdate']
  }
}